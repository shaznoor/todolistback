import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './user.model';
import { MaintodoService } from '../maintodo/maintodo.service';
import { SubtodoService } from 'src/subtodo/subtodo.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    private readonly mainTodoService: MaintodoService,
    private readonly subTodoService: SubtodoService,
  ) {}

  async insertUser(name: string, email: string, password: string) {
    const findEmail = await this.userModel.findOne({ email });
    if (findEmail) {
      throw new HttpException(
        'Wrong credentials provided',
        HttpStatus.BAD_REQUEST,
      );
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = new this.userModel({
      name: name,
      email: email,
      password: hashedPassword,
    });
    const userId = await newUser.save();
    return userId.id;
  }

  async findOne(email: string) {
    const user = await this.userModel.findOne({ email });
    return user;
  }

  async insertMainTodo(id: string, title: string) {
    const newMainTodo = await this.mainTodoService.insertMainTodo(title);
    const user = await this.userModel.findById(id);
    user.Todo.push(newMainTodo);
    const res = await user.save();
    return await this.getTodoUser(id);
  }

  async insertSubTodo(userid: string, maintodoid: string, title: string) {
    const newSubTodo = await this.subTodoService.insertSubTodo(title);
    const user = await this.userModel.findById(userid);
    const todo = user.Todo;
    const subtodo = todo.find((e) => maintodoid == e.id);
    subtodo.SubTodo.push(newSubTodo);
    const res = await user.save();
    return await this.getTodoUser(userid);
  }

  async getTodoUser(userId: string) {
    const user = await this.userModel.findById(userId);
    return user.Todo;
  }

  async deleteMainTodo(userId: string, maintodoId: string) {
    const user = await this.userModel.findById(userId);
    let userTodo = user.Todo;
    userTodo = userTodo.filter((value) => value.id != maintodoId);
    user.Todo = userTodo;
    const res = await user.save();
    return await this.getTodoUser(userId);
  }

  async deleteSubTodo(userId: string, maintodoId: string, subtodoId: string) {
    const user = await this.userModel.findById(userId);
    let a = undefined;
    let b = undefined;
    for (let i = 0; i < user.Todo.length; i++) {
      if (user.Todo[i].id == maintodoId) {
        for (let j = 0; j < user.Todo[i].SubTodo.length; j++) {
          if (user.Todo[i].SubTodo[j].id == subtodoId) {
            a = i;
            b = j;
            break;
          }
        }
      }
    }
    if (a != undefined && b != undefined) {
      user.Todo[a].SubTodo.splice(b, 1);
    }
    let res = await user.save();
    return await this.getTodoUser(userId);
  }
}
