import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { MainTodoSchema, MainTodo } from '../maintodo/maintodo.model';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  Todo: [MainTodoSchema],
});

export interface User extends Document {
  id: string;
  name: string;
  email: string;
  password: string;
  Todo: MainTodo[];
}
