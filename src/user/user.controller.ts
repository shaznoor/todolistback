import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { AuthService } from 'src/auth/auth.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('gettodo')
  async getTodoUser(@Request() req) {
    return await this.userService.getTodoUser(req.user.userId);
  }

  @Post()
  async addUser(
    @Body('name') userName: string,
    @Body('email') userEmail: string,
    @Body('password') userPassword: string,
  ) {
    return await this.userService.insertUser(userName, userEmail, userPassword);
  }

  @UseGuards(JwtAuthGuard)
  @Post('maintodo')
  async addMainTodo(@Request() req) {
    return await this.userService.insertMainTodo(
      req.user.userId,
      req.body.title,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('subtodo')
  async addSubTodo(@Request() req) {
    return await this.userService.insertSubTodo(
      req.user.userId,
      req.body.maintodoid,
      req.body.title,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Put('deletemaintodo')
  async deleteMainTodo(@Request() req) {
    return await this.userService.deleteMainTodo(
      req.user.userId,
      req.body.maintodoid,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Put('deletesubtodo')
  async deleteSubTodo(@Request() req) {
    return await this.userService.deleteSubTodo(
      req.user.userId,
      req.body.maintodoid,
      req.body.subtodoid,
    );
  }
}
