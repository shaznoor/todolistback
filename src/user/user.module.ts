import { forwardRef, Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.model';
import { MaintodoModule } from '../maintodo/maintodo.module';
import { SubtodoModule } from 'src/subtodo/subtodo.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    MaintodoModule,
    SubtodoModule,
    forwardRef(() => AuthModule),
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
