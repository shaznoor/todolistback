import * as mongoose from 'mongoose';
import { Document } from 'mongoose';

export const SubTodoSchema = new mongoose.Schema({
  title: { type: String, required: true },
});

export interface SubTodo extends Document {
  id: string;
  title: string;
}
