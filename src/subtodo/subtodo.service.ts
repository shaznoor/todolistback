import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SubTodo } from './subtodo.model';

@Injectable()
export class SubtodoService {
  constructor(
    @InjectModel('SubTodo') private readonly subTodoModel: Model<SubTodo>,
  ) {}

  async insertSubTodo(title: string) {
    const newSubTodo = new this.subTodoModel({
      title: title,
    });
    return newSubTodo;
  }
}
