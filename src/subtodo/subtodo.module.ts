import { Module } from '@nestjs/common';
import { SubtodoController } from './subtodo.controller';
import { SubtodoService } from './subtodo.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SubTodoSchema } from './subtodo.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SubTodo', schema: SubTodoSchema }]),
  ],
  controllers: [SubtodoController],
  providers: [SubtodoService],
  exports: [SubtodoService],
})
export class SubtodoModule {}
