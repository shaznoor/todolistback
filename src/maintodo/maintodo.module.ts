import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MaintodoController } from './maintodo.controller';
import { MaintodoService } from './maintodo.service';
import { MainTodoSchema } from './maintodo.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'MainTodo', schema: MainTodoSchema }]),
  ],
  controllers: [MaintodoController],
  providers: [MaintodoService],
  exports: [MaintodoService],
})
export class MaintodoModule {}
