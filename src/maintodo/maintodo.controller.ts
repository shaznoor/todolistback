import { Body, Controller, Post } from '@nestjs/common';
import { MaintodoService } from './maintodo.service';

@Controller('maintodo')
export class MaintodoController {
  constructor(private readonly mainTodoService: MaintodoService) {}

  @Post()
  async addMainTodo(
    @Body('title') mainTodoTitle: string,
  ) {
    return await this.mainTodoService.insertMainTodo(mainTodoTitle);
  }
}
