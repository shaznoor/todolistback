import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { SubTodoSchema,SubTodo } from '../subtodo/subtodo.model';

export const MainTodoSchema = new mongoose.Schema({
  title: { type: String, required: true },
  SubTodo: [SubTodoSchema],
});

export interface MainTodo extends Document {
  id: string;
  title: string;
  SubTodo: SubTodo[];
}
