import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MainTodo } from './maintodo.model';

@Injectable()
export class MaintodoService {
  constructor(
    @InjectModel('MainTodo') private readonly mainTodoModel: Model<MainTodo>,
  ) {}

  async insertMainTodo(title: string){
      const newMainTodo=new this.mainTodoModel({
          title:title,
      })
      return newMainTodo;
  }
}
